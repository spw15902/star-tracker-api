# Konfiguration der Projektinformationen
project = 'Star Tracker API'
author = 'Seybi'
version = '0.1'
release = '0.1.1'

# Sphinx-Erweiterungen und Themes
extensions = [
    'sphinx.ext.autodoc',
    'sphinx.ext.todo',
    'sphinx.ext.viewcode'
]
html_theme = 'sphinx_rtd_theme'

# Verzeichnisse und Ausschlüsse
exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store']
